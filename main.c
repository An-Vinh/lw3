#include <avr/io.h>
#include <util/delay.h> 
#include <avr/interrupt.h>




/*Make the correct configuration of timer/counter0.
Use overflow interrupts and "normal"
signal generation mode. Use a divider equal to 256. 
Demonstrate to the teacher 
that the overflow interrupt works using the LED,
which is connected to PB2. Output the timer
counter data to the terminal.*/


/*



volatile int stop = 0;


int main () { 
  
TCCR0B = 1<<CS02;

TCNT0 =0;

TIMSK0 = 1<<TOIE0;
Serial.begin(9600);
 DDRB = 0b111111;
 sei();

    while(1){
      

  }

}

ISR(TIMER0_OVF_vect){
     stop+=1;
  Serial.println(stop+'\n');
  if(stop >= 244){
        PORTB ^= 1<<PB2;
        stop = 0;
   		TCNT0 = 0;
      }
}
    
  */



//------------------------Q2-----------------------

/*

Make the correct configuration of the timer/counter1 
with compare match interrupt in channel A.
Use the fast PWM waveform generation mode.
Use a divider equal to 1024.
Demonstrate to the teacher 
that the compare match interrupt in channel A works using the
LED,
which is connected to PB3.
Output the timer counter data to the terminal.
*/

/*
volatile long int overf1 = 0;

int main () 
{ 
    TCCR1A |= (1<<WGM11)|(1<<WGM10);
    TCCR1B |= (1<<CS10)|(1<<CS12)|(1<<WGM12)|(1<<WGM13);
    TCNT1 = 0;
    OCR1A = 144;
    TIMSK1 |= (1<<OCIE1A);
    DDRB = 0b111111;
      sei();
      Serial.begin(9800);
      while(1)
    {

    }
}


ISR(TIMER1_COMPA_vect)
{
  overf1++;
  Serial.println(overf1);
  if (overf1 > 200)
          {
              PORTB ^= 1<<PB3; 
             overf1 = 0;
          }

}



*/
