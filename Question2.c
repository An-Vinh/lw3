#include <avr/io.h>
#include <util/delay.h> 
#include <avr/interrupt.h>



volatile long int overf1 = 0;

int main () 
{ 
    TCCR1A |= (1<<WGM11)|(1<<WGM10);
    TCCR1B |= (1<<CS10)|(1<<CS12)|(1<<WGM12)|(1<<WGM13);
    TCNT1 = 0;
    OCR1A = 144;
    TIMSK1 |= (1<<OCIE1A);
    DDRB = 0b111111;
      sei();
      Serial.begin(9800);
      while(1)
    {

    }
}


ISR(TIMER1_COMPA_vect)
{
  overf1++;
  Serial.println(overf1);
  if (overf1 > 200)
          {
              PORTB ^= 1<<PB3; 
             overf1 = 0;
          }

}


